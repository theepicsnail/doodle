/** @type {HTMLCanvasElement} */
const canvas = document.getElementById("doodle"); 
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
const ctx = canvas.getContext('2d');

ctx.fillStyle = "black";
ctx.fillRect(0,0,canvas.width, canvas.height);


ctx.translate(canvas.width/2, canvas.height/2);

function doodler() {
    requestAnimationFrame(doodler);
    
    let now = Date.now()*.001;

    h = (now*30);
    h += Math.random()*10;
    h %= 360;
    l = Math.random()*50+50;
    ctx.strokeStyle=`hsl(${h},100%,${l}%)`;
    
    ctx.save();
    ctx.beginPath();
    let max = Math.max(canvas.width, canvas.height)
    for(let i = 0 ; i < max; i += max/10) {
        if(Math.random() < .1) {
        r = i;
        phase = Math.random() * 6.28;
        ctx.arc(0,0,r,phase,6.28+phase);
        }   
    }
    ctx.stroke();
    ctx.restore();

    
    ctx.save();
    ctx.beginPath();
    ctx.translate(Math.random()*600-300,Math.random()*600-300);
    ctx.rect(-10,-10,20,20);
    ctx.stroke();
    ctx.restore()


    ctx.save();
    h = (now);
    h %= 360;
    ctx.lineWidth=1;
    now *= .5;
    for(i = 0 ; i < 10 ; i++) {
        ctx.beginPath();
        l = noise(now+i*.1)*0+50;
        h = noise(now)*180+180;
        ctx.strokeStyle=`hsl(${h},100%,${l}%)`;
        now += .1
        x=  noise(now+1)*canvas.width*.35
        y = noise(now+3)*canvas.height*.35
        w = noise(now+2)*canvas.width*.15
        h = noise(now+4)*canvas.height*.15
        ctx.moveTo(x,y);
        ctx.lineTo(x+w,y+h);
        ctx.stroke();
    }
    ctx.restore()

    
    postprocess()
}
function postprocess(){
    

    let now = Date.now()*.001;

    h = (now);
    h += Math.random()*120;
    h %= 360;

    ctx.save();
    //ctx.translate(canvas.width/2, canvas.height/2);
    //ctx.rotate(.1 * Math.cos(h/360*3.14*2));
    now *= .25
    s = 2;
    ctx.translate(s*noise(now+100), s*noise(now+200));

    ctx.rotate(.1*noise(now*4+10.3));
    now *= .25;
    r = .1
    m = 1.0
    ctx.scale(m +  r*noise(now*11+300), m+ r* noise(now*10+45));

    ctx.drawImage(canvas,-canvas.width/2,-canvas.height/2);
    ctx.restore();

    ctx.fillStyle=`rgba(0,0,0,${.05*noise(now)+.05})`
    ctx.fillRect(-canvas.width/2,-canvas.height/2,canvas.width, canvas.height);
}

function hash(x) { return Math.sin(Math.sin(x*x*38)*135);}
function noise(x) {
    let left = Math.floor(x);
    let right = Math.ceil(x);
    let percent = x-left;
    left = hash(left);
    right = hash(right);
    return left + (right-left)*percent;
}
doodler();
